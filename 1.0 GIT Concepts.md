# GIT Concepts

- `git status`
- To restore the last git add before commit:
	- Let's say, names.txt was staged after git add, to restore:
		- `git restore --staged names.txt`

- `git add names.txt`
- `git log`  <-- Check the git history
- To go back to particular commit: `git reset <commit_no_from_git_log`
	- All the commits above it will be unstaged

- Go to backstage which will be called when required
	- Don't commit or set in history
	- `git stash`
	- Now, git status shows clean
	- To get back the changes from the stash: `git stash pop`
	- To remove everything from the stash: `git stash clear`

- Add remote git: `git rmeote add origin <git_URL>`
- `git remote -v`


## Branching

Every commit statement creates a branch
Never commit something on the main branch

- git commit
- git branch feature
- git checkout feature
- git commit
- git checkout main
- git commit
- git merge feature
- git push origin master


## How to contribute?

- Fork the project to contribute
- Clone the project & make necessary changes

The origin URL is our own account
From where the project is forked, that URL is upstream

- `git remote add upstream <mail_url_from_where_project_was_forked>`
- `git remote -v`
- `git status`
- `git branch chirag` // Create a new branch
- `git checkout chirag` // Change head to chirag branch
- git add .
- git commit -m "Update by Chirag"
- git log
- To generate full request to merged the changed code to the main branch of the upstream account:
	- git push origin chirag 	// Push to self project NOT on original project
	- Open Github --> Compare and Pull Request
	- Create pull request
	- Owner can merge the pull request
	- Now if we make more changes it will be added in the commit pull request already created SO WE SHOULD NEVER COMMIT ON MAIN BRANCH
	- One branch can open only 1 pull request
- To remove a commit
	- git log
	- `git reset <commit_no_below_toremove_code>` // Change to unstage
	- git add .
	- git stash
	- git log
	- git push origin chirag -f  // force push

- To update the fork on the self-accout
	- Fetch upstream in github        OR,
	- git checkout main
	- git fetch --all --prune
	- git checkout main
	- git reset --hard upstream/main
	- git log
	- git push origin main

- git pull upstream main


> Squash commits:

git add .; git commit -m "Updated"; git push origin main

`git rebase -i <lowest_git_code_from_git_log>`

Replace 'pick' with 's' to squash except the 1st one which will merge the s commits into the pick commit

Merges the s to the pick above it
```
pick bjbhii 1
s bjhgii 2
s jinjkk 3
pick huhhjh 4
```
 Above code will merge 2 and 3 in 1 as 4 will remain as it si
 
 
 ## Merge Conflicts
 
 - 2 people modified same line number on same file whicch change to take