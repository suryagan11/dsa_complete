package com.chirag;

import java.util.Arrays;
import java.util.Scanner;

public class Input {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        // Array of Primitives
        int[] arr = new int[5];
        arr[0] = 23;
        arr[1] = 45;

        System.out.println(arr[1]);

        // input using for
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }

        // for-each loop
        for (int j : arr) {     // for j in elements of arr
            System.out.println(j + " ");
        }

        System.out.println(Arrays.toString(arr));   // convert an array into a string & prints it

        // Array of Objects
        String[] str = new String[4];
        for(int i = 0; i < str.length; i++){
            str[i] = in.next();
        }
        System.out.println(Arrays.toString(str));

        // Modify Array Elements
        str[1] = "Chirag";
        System.out.println(Arrays.toString(str));
    }
}
