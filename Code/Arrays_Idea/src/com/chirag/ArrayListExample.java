package com.chirag;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListExample {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Syntax
        ArrayList<Integer> list = new ArrayList<>(10);

        list.add(67);
        list.add(92);
        list.add(31);

        // add as many as you want (more than 10 is ok)

        System.out.println(list);

        // Search contains
        list.contains(67);

        // modify
        list.set(0, 99);    // Replace 0th index with 99
        System.out.println(list);

        // remove
        list.remove(2);     // remove 2nd index item
        System.out.println(list);

        // iterate
        for (int i = 0; i < 5; i++) {
            list.add(in.nextInt());
        }
        System.out.println(list);

        // get item @ any index
        for (int i = 0; i < 5; i++) {
            System.out.println(list.get(i));    // Pass index here
        }
    }
}

/*
1. Size is fixed internally
2. SAy when ArrayList fills by some amount, It will create a new ArrayList of say, double the size
3. Old elements are copied to the new list and the old one is deleted
4. Amortized time complexity O(1) Constant time complexity
 */