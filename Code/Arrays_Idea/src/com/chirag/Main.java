package com.chirag;

public class Main {

    public static void main(String[] args) {
	    // Store a roll number
        int a = 19;

        // Store a person's name
        String name = "Chirag";

        // datatype[] variable_name = new datatype[size];

        // store 5 roll no
        int[] rnos = new int[5];
        // or,
        int[] rnos2 = {23, 12, 45, 21, 11};

        // All types of data in an array must be same

        int[] ros;  // declaration of array. ros is getting defined in the stack
        ros = new int[5];   // object is being created in the heap memory | initialization
        // [0,0,0,0,0]
        System.out.println(ros[1]); // 0

        String[] arr = new String[4];
        // [null,null,null,null]
        System.out.println(arr[0]); // null
    }
}

/*
 In java, ww have no concept of pointers
 Array objects are in heap
 Heap objects are not continuous
 Dynamic Memory Allocation
 Hence: Array Objects in JAVA ay not be Continuous! | Depends on JVM
*/

// new is used to create an object

// primitives are stored in the stack memory only
// All other objects like string, array are stored in the heap memory
