package com.chirag;

import java.util.Arrays;
import java.util.Scanner;

public class MultiDimension {
    public static void main(String[] args) {
       // 2-D Array
       /*
            1 2 3
            4 5 6
            7 8 9
        */

        Scanner sc = new Scanner(System.in);

        int[][] arr = new int[3][]; // [rows][columns]
        // Mention no of rows is mandatory but not columns

        int[][] arr1 = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };

        int[][] arr2 = {
                {1,2,3},    // 0th index
                {4,5},      // 1st index
                {6,7,8,9}   // 2nd index
        };

        // In 2-D array, C, C++ its continuous memory allocation but not in Java
        // Multidimensional array can be considered as an array of arrays in the memory in heap

        int[][] array = new int[3][3];
        System.out.println(arr.length); // no of rows

        // Input
        for(int row = 0; row < array.length; row++) {
            // for each col in every row
            for(int col = 0; col < array[row].length; col++) {
                array[row][col] = sc.nextInt();
            }
        }

        // Output
        for(int row = 0; row < array.length; row++) {
            // for each col in every row
            for(int col = 0; col < array[row].length; col++) {
                System.out.print(array[row][col] + " ");
            }
            System.out.println();
        }

        // Output 2
        for(int row = 0; row < array.length; row++){
            System.out.println(Arrays.toString(array[row]));
        }

        for(int[] a : array) {
            System.out.println(Arrays.toString(a));
        }
    }
}
