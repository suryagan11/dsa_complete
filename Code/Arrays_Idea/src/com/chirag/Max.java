package com.chirag;

public class Max {
    public static void main(String[] args) {
        int[] arr = {1,3,23,9,18};

        System.out.println(max(arr));
        System.out.println(maxRange(arr,3,4));
    }

    // find max value in a given array
    static int max(int[] arr) {

        if(arr.length == 0) {   // edge case
            return -1;
        }

        int max = arr[0];
        // int max = Integer.MIN_VALUE;  // is array is empty
        for (int i = 1; i < arr.length; i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    // find max value in a range of array
    static int maxRange(int[] arr, int start, int end) {

        if(end > start){    // edge case
            return -1;
        }

        if(arr == null) {   // edge case
            return -1;
        }

        int max = arr[start];
        // int max = Integer.MIN_VALUE;  // is array is empty
        for (int i = start; i <= end; i++) {
            if(arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
