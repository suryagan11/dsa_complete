package com.chirag;

// Position of an Element In Infinite Sorted Array
// https://www.geeksforgeeks.org/find-position-element-sorted-array-infinite-numbers/

public class ElemPositionInInfiniteSortedArray {
    public static void main(String[] args) {
        // Binary Search in Sorted Array
        // Take chunks of element & check present of elem, then double the chunk size
        // When the search is present in a range, perform BS to find the index of elem

        int[] arr = {3,5,7,9,10,99,100,130,140,160,170};
        int target = 10;
        System.out.println(ans(arr, target));
    }

    static int ans(int[] arr, int target) {
        // start with the chunk of size 2
        int start = 0;
        int end = 1;

        // condition for the target to lie in the range
        while(target > arr[end]) {
            int temp = end + 1;
            // double the chunk value
            // end = previous end + chunkSize * 2
            end = end + (end - start + 1) * 2;
            start = temp;
        }
        return binarySearch(arr, target, start, end);
    }

    static int binarySearch(int[]arr, int target, int start, int end) {

        while(start <= end) {
            // find middle element
            // int mid = (start + end) / 2; might be possible start+end exceeds the range of int in java
            int mid = start + ((end - start) / 2);

            if(target < arr[mid]) {
                // search left side
                end = mid - 1;
            } else if(target > arr[mid]) {
                // search right side
                start = mid + 1;
            } else {
                // ans found
                return mid;
            }
        }
        return -1;
    }
}
