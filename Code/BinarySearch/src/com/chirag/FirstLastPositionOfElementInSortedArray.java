package com.chirag;

// https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
public class FirstLastPositionOfElementInSortedArray {
    public static void main(String[] args) {
        // arr[] = {5,7,7,7,7,8,8,10}  target: 7
        // find 1st & last position of 7
        // 1st: 1   last: 4 => [1,4]

        // Start one from beginning & one from end => Bruteforce technique O(n)

        // find 1st occurrence of 7 -> BS -> end = mid-1
        // find last occurrence of 7 -> BS -> start = mid+1
    }

    public int[] searchRange(int[] nums, int target) {

        int[] ans = {-1, -1};

        // check first occurrence of index
        ans[0] = search(nums, target, true);
        if(ans[0] != -1) {
            ans[1] = search(nums, target, false);
        }

        return ans;
    }

    // this function returns the index of target

    int search(int[] nums, int target, boolean findStartIndex) {

        int ans = -1;
        int start = 0;
        int end = nums.length - 1;
        while(start <= end) {
            // find middle element
            // int mid = (start + end) / 2; might be possible start+end exceeds the range of int in java
            int mid = start + ((end - start) / 2);

            if (target < nums[mid]) {
                // search left side
                end = mid - 1;
            } else if (target > nums[mid]) {
                // search right side
                start = mid + 1;
            } else {
                // potential ans found
                ans = mid;
                if(findStartIndex) {
                    end = mid-1;
                } else {
                    start = mid+1;
                }
            }
        }
        return ans;
    }
}
