package com.chirag;

public class FloorOfANo {
    public static void main(String[] args) {
        // Floor: Greatest number < or = to the target number
        // arr[] = {2,3,5,9,14,16,18}
        // floor(arr, target=15) = 14

        // same as ceil & bin Search but return end instead of -1 whe no is not found

        int[] arr = {2,3,5,9,14,16,18};
        int target = 15;
        int ans = floor(arr, target);
        System.out.println(ans);
    }

    static int floor(int[]arr, int target) {

        int start = 0;
        int end = arr.length - 1;
        while(start <= end) {
            // find middle element
            // int mid = (start + end) / 2; might be possible start+end exceeds the range of int in java
            int mid = start + ((end - start) / 2);

            if(target < arr[mid]) {
                // search left side
                end = mid - 1;
            } else if(target > arr[mid]) {
                // search right side
                start = mid + 1;
            } else {
                // ans found
                return mid;
            }
        }
        return end;     // ONLY CHANGE!
    }
}

// O(log n) time complexity