package com.chirag;

public class OrderAgnosticBinarySearch {
    public static void main(String[] args) {
        // if start < end --> increasing order sorted else decreasing order sorted
        int[] arr = {-18, -12,-4,0,2,3,4,15,16,18,22};
        int[] arr2 = {99,80,75,22,11,10,5,2,-3};
        int target = 22;

        int ans = orderAgnosticBS(arr, target);
        System.out.println(ans);
        int ans2 = orderAgnosticBS(arr2, target);
        System.out.println(ans2);
    }

    static int orderAgnosticBS(int[] arr, int target) {
        int start = 0;
        int end = arr.length - 1;

        // find whether the array is sorted in asc or desc
        boolean isAsc;
        isAsc = arr[start] < arr[end];

        while(start <= end) {
            // find middle element
            // int mid = (start + end) / 2; might be possible start+end exceeds the range of int in java
            int mid = start + ((end - start) / 2);

            if(arr[mid] == target) {
                return mid;
            }

            if(isAsc) {
                if (target < arr[mid]) {
                    // search left side
                    end = mid - 1;
                } else {
                    // search right side
                    start = mid + 1;
                }
            } else {
                if(target > arr[mid]) {
                    // search left side
                    end = mid - 1;
                } else {
                    // search right side
                    start = mid + 1;
                }
            }
        }
        return -1;
    }
}
