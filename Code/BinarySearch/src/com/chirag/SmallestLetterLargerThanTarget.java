package com.chirag;

// https://leetcode.com/problems/find-smallest-letter-greater-than-target/
public class SmallestLetterLargerThanTarget {
    public static void main(String[] args) {
        // same as ceiling of a number
        // ignore the target = what we are looking for
        // Letters wrap around | arr[] = {'c','e','f','j'}, target = 'j'
        // --> no char larger than target 'j' so return 'c' | can be done by modulo
        // condition violated: start = end+1 => len(array) = N
        // return start % len(array)=N --> 1st position


    }

    public char nextGreatestLetter(char[] letters, char target) {

        int start = 0;
        int end = letters.length - 1;
        while(start <= end) {
            // find middle element
            // int mid = (start + end) / 2; might be possible start+end exceeds the range of int in java
            int mid = start + ((end - start) / 2);

            if(target < letters[mid]) {
                // search left side
                end = mid - 1;
            } else {
                // search right side
                start = mid + 1;
            }
        }
        return letters[start % letters.length];     // return letter at index instead of just the index
    }
}
