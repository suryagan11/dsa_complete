package com.chirag;

// https://leetcode.com/problems/peak-index-in-a-mountain-array/
// https://leetcode.com/problems/find-peak-element/
public class PeakIndex {
    public static void main(String[] args) {

    }

    public int peakIndex(int[] arr) {
        int start = 0;
        int end = arr.length - 1;

        while(start < end) {
            int mid = start + (end-start) / 2;
            if(arr[mid] > arr[mid+1]){
                end = mid;
            } else {
                start = mid+1;
            }
        }
        return start;
    }
}

// arr[] = {1,2,3,5,7,6,3,2}
// 1-7 increasing & then 6 to 2 decreasing (Mountain Array) --> Bitonic Array
// TO search 7 which is at the top of the mountain peak
// Finding max no is the increasing order (in array) is the ANSWER!

// 2:03:03