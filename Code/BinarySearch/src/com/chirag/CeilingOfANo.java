package com.chirag;

public class CeilingOfANo {
    public static void main(String[] args) {
        // Ceiling: smallest elem in array > or = to target elem
        // arr = [2,3,5,9,14,16,118]
        // ceiling(arr, target=14) = 14
        // ceiling(arr, target=15) = 16
        // ceiling(arr, target=4) = 5
        // ceiling(arr, target=9) = 9

        // SORTED ARRAY =-> Apply BINARY SEARCH

        // start <= end --> when while loop breaks start = end + 1
        // next big no found = start element

        // Same as Binary Search but will return start when the elem is not found or start > end

        int[] arr = {2,3,5,9,14,16,18};
        int target = 15;
        int ans = ceiling(arr, target);
        System.out.println(ans);
    }

    // return index of the smallest no >= target
    static int ceiling(int[]arr, int target) {

        // what is target is greater than greatest no in array
        if(target > arr[arr.length-1]) {
            return -1;
        }

        int start = 0;
        int end = arr.length - 1;
        while(start <= end) {
            // find middle element
            // int mid = (start + end) / 2; might be possible start+end exceeds the range of int in java
            int mid = start + ((end - start) / 2);

            if(target < arr[mid]) {
                // search left side
                end = mid - 1;
            } else if(target > arr[mid]) {
                // search right side
                start = mid + 1;
            } else {
                // ans found
                return mid;
            }
        }
        return start;   // ONLY CHANGE!
    }
}

// O(log n) time complexity