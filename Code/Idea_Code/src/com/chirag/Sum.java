package com.chirag;

import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("First num: ");
        int num1 = input.nextInt();
        System.out.println("Second num: ");
        int num2 = input.nextInt();

        int sum = num1 + num2;

        System.out.println("Sum: " + sum);
    }
}
