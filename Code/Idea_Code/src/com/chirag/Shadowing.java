package com.chirag;

// Shadowing is the practice of using 2 variables with the sane name within the scope that overlaps

public class Shadowing {
    static int x = 90;  // shadowed at line 13
    public static void main(String[] args) {
        System.out.println(x);  // 90

        fun();  // 90
        // Scope will begin when the value is initialized
        int x = 40; // the class variable in line 6 is shadowed
        System.out.println(40); // 40
    }

    static void fun() {
        System.out.println(x);
    }
}
