package com.chirag;

import java.util.Arrays;

public class ChangeValue {
    public static void main(String[] args) {
        // create an array
        int[] arr = {1,2,3,45,6};

        change(arr);    // ref passed by Call By Value
        System.out.println(Arrays.toString(arr));
    }

    static void change(int[] nums) {
        nums[0] = 99;
        // Original array is changes
        // Array is modified unlike strings which are immutable
        // If you make a change to the object via ref variable, same object will change
    }
}
