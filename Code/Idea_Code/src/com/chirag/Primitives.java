package com.chirag;

public class Primitives {
    public static void main(String[] args) {
        int rollno = 60;  // int starts with small lttr so it's a primitive data type not class
        String name = "chirag"; // string is not a primitive data type
        char letter = 'c';
        float marks = 99.21f;
        double largeDecimalNumbers = 45666766.23;
        long largeIntegerValue = 5666657757L;
        boolean check = true;

        Integer rollnos = 60;   // wrapper class
        rollnos.hashCode();
    }
}
