package com.chirag;

import java.util.Scanner;

public class TypeCasting {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        float num = input.nextFloat();
        System.out.println(num);
        // On input of int it gets auto-converted to float
        // Happens when 2 types are compatible & dest type should be greater than src type
        // Therefore, we can say int cannot be auto-converted to float

        // To narrow float to int (Narrowing conversion)
        int number = (int)(67.56f);    // type casting
        System.out.println(number);

        // Automatic type promotions in expression
        int a = 257;
        byte b = (byte)a;
        System.out.println(b); // Out: 1 as the max range for byte is 256 | 257 % 256 = 1

        byte c = 40;
        byte d = 50;
        byte e = 100;
        int f = (c * d) / e;
        System.out.println(f); // output: 20 | True
        // Result of a * b exceeds 256 so java auto promotes the byte expression to int to save the intermediate

        int nos = 'a';
        System.out.println(nos); // ASCII Value auto type-conversion

        System.out.println("नमस्ते চিরাগ"); // Unicode values can have literally anything!

        // All byte, short & char values are promoted to int
        // If any 1 of the operands is long / float / double, the whole operation to long / float / double
        System.out.println(3 * 14.6788901f); // int * float = float

        // Summary
        byte t1 = 42;
        char c1 = 'a';
        short t2 = 1024;
        int t3 = 50000;
        float t4 = 5.67f;
        double t5 = 0.1234;
        double result = (t4 * t1) + (t3 / c1) - (t5 - t2);
        System.out.println((t4 * t1) + " " + (t3 / c1) + " " + (t5 - t2));
        System.out.println(result);
        // float + int - double = double
    }
}
