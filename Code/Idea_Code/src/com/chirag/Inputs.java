package com.chirag;

import java.util.Scanner;

public class Inputs {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your roll no: ");
        int rollno = input.nextInt();
        System.out.println("Your roll no is: " + rollno);

        // 234 million can be written as (instead of using comma in between)
        int a = 234_000_000;
        System.out.println(a);

        System.out.print("Enter yur marks: ");
        float marks = input.nextFloat();
        System.out.println(marks);
        // If we input integer here: Type casting / conversion
    }
}
