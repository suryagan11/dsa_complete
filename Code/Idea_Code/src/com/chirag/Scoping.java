package com.chirag;

public class Scoping {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        // a can be accessed her but NOT num or nums
        String name = "Chirag";

        System.out.println(a);
        System.out.println(name);

        {
            // ||||| BLOCK SCOPING |||||

            a = 78; // Already init outside the block in same method so can't be init again but value can be changed
            // a is reassigned (original value) to some other value
            System.out.println(a);
            int c = 99;

            name = "Ganguli";
        }

        // Cannot initialize the same variable a again inside the block inside the same function where it is already initialized
        // System.out.println(c); | ERROR
        // Values initialized inside a block will remain inside the block
        int c = 100; // Re-initialized outside the block
        System.out.println(a);

        System.out.println(name);


        // ||||| FOR LOOP SCOPING |||||

        for (int i = 0; i < 4; i++) {
            System.out.println(i);  // i defined only in for loop
        }
        // System.out.println(i); | ERROR
    }

    static void random(int nums) {
        // ||||| FUNCTION SCOPE |||||

        // System.out.println(a);
        // a can't be accesses here

        int num = 67;
        // num can be accessed here
    }
}

// Anything used OUTSIDE can be used INSIDE
// Anything used INSIDE cannot be used OUTSIDE