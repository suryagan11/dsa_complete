package com.chirag;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {

        // 0,1,1,2,3,5,8,13,21,...
        // Find the nth fibonacci number

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int p = 0, i = 1;
        int count = 2;

        while(count <= n) {
            int temp = i;
            i = i + p;
            p = temp;
            count++;
        }

        System.out.println(i);
    }
}
