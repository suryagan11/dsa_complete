package com.chirag;

import java.util.Scanner;

public class FunctionsMethods {
    public static void main(String[] args) {
        // take input of 2 nos & get the sum
        greeting();
        sum();
        int ans = sum2();
        System.out.println(ans);

        int ans3 = sum3(20, 30);
        System.out.println(ans3);
    }

    // return the value
    static int sum2(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter num1: ");
        int num1 = sc.nextInt();
        System.out.print("Enter num2: ");
        int num2 = sc.nextInt();
        int sum = num1 + num2;
        return sum;
    }

    static void sum(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter num1: ");
        int num1 = sc.nextInt();
        System.out.print("Enter num2: ");
        int num2 = sc.nextInt();
        int sum = num1 + num2;
        System.out.print("Sum = " + sum);
        System.out.println();
    }

    // pass the value of numbers when calling the method in main

    static int sum3(int a, int b) {
        int sum = a + b;
        return sum;
    }

    static void greeting(){
        System.out.println("Hello World!");
    }
    /*
        access modifier return_type name() {
            // body
            return statement
        }
     */
}

// 14:25