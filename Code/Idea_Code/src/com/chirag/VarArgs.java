package com.chirag;

// Variable Length Arguments

import java.util.Arrays;

public class VarArgs {
    public static void main(String[] args) {
        fun(2,3,4,5,67,89,12,2,22); // as many as possible
        multipleArg(2,3,"Chirag", "Kunal", "Rahul");

        demo(2,3,4);
        demo("Chirag", "Kunal");
    }

    static void fun(int ...v) {     // ...v takes as an array of integers
        System.out.println(Arrays.toString(v));
    }

    static void multipleArg(int a, int b, String ...v){
        System.out.println();
    }

    static void demo(int ...v) {
        System.out.println(Arrays.toString(v));
    }

    static void demo(String ...v) {
        System.out.println(Arrays.toString(v));
    }
}

// ...v can not only be int but also strings
// Variable length arguments / parameter must be the last in the list