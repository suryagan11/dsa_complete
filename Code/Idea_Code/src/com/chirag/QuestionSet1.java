package com.chirag;

import java.util.Scanner;

public class QuestionSet1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        System.out.println(isPrime(n));
    }

    static boolean isPrime(int n) {
        // Square Root Method to find if entered number is prime
        if (n <= 1) {
            return false;
        }
        int c = 2;
        while(c * c <= n) {
            if(n % c == 0) {
                return false;
            }
            c++;
        }

        return c * c > n;   // if yes returns true else  false4
    }
}