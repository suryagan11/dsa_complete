package com.chirag;

import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String fruit = in.next();

        // Process 1

//        if(fruit.equals("mango")) {
//            System.out.println("King of fruit");
//        }
//
//        if(fruit.equals("apple")) {
//            System.out.println("a sweet fruit");
//        }

        // Process 2

        switch (fruit) {
            case "mango" -> System.out.println("King of fruit");
            case "apple" -> System.out.println("a sweet fruit");
            default -> System.out.println("Invalid fruit");
        }

        int day = in.nextInt();

        switch (day) {
            case 1, 2, 3, 4, 5 -> System.out.println("Weekday");
            case 6, 7 -> System.out.println("Weekend");
        }

        // Nested Switch

        int empID = in.nextInt();
        String dept = in.next();

        switch (empID) {
            case 1 -> System.out.println("Chirag Ganguli");
            case 2 -> System.out.println("Kunal Kushwaha");
            case 3 -> {
                System.out.println(" Emp No 3 ");
                switch (dept) {
                    case "IT" -> System.out.println("IT Department");
                    case "Management" -> System.out.println("Management Department");
                    default -> System.out.println("No departments chosen");
                }
            }
            default -> System.out.println("Enter correct Employee ID");
        }

    }
}
