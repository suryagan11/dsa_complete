package com.chirag;

public class Basics {
    public static void main(String[] args) {
        if (true) {
            System.out.println("Hello Chirag!");
        }

        int a = 10;
        if(a == 10) {
            System.out.println("Hey, a is 10!");
        }

        int count = 1;
        while(count != 5) {
            System.out.println(count);
            count++;
        }

        for(int i = 1; i != 5; i++) {
            System.out.println(i);
        }

        int n = 1;
        do {
            System.out.println(n);
            n++;
        } while (n <= 5);

        int set = 1;
        do {
            System.out.println("Hello Looping");
        } while (set != 1);    // should not execute but prints once as do is an entry controlled loop
    }
}
