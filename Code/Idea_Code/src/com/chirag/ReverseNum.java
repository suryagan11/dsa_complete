package com.chirag;

import java.util.Scanner;

public class ReverseNum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // Reverse an integer number

//        23597 --> reverse: 79532
//        ans = 7 * 10 + 9 = 79
//            = 79 * 10 + 5 = 795
//            = 795 * 10 + 3 = 7953
//            = 7953 * 10 + 2 = 79532

        int num = sc.nextInt();
        int ans = 0;

        while (num > 0) {
            int rem = num % 10;
            num /= 10;
            ans = ans * 10 + rem;
        }

        System.out.println(ans);
    }
}
