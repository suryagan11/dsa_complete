package com.chirag;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hey!");
        // sout: system.out.print
        // psvm: public static void main
        System.out.println("How are you?");
        Scanner input = new Scanner(System.in);
        System.out.println(input.next());
        // prints only the 1st word in a line
        System.out.println(input.nextLine());
        //prints entire line
    }
}
