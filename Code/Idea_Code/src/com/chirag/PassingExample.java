package com.chirag;

public class PassingExample {
    public static void main(String[] args) {
        String name = "Chirag Ganguli";
        greet(name);    // When name is passed in the method the value is passed
        // In Java, there's nothing called pass by reference
    }

    private static void greet(String naam) {
        System.out.println(naam);
    }

    // naam cannot be changed outside the greet function
    // This process of a variable to be usable only inside its function: SCOPING
}

// Refer ChangeValue.java for more info

// For Scoping, refer Scoping.java