package com.chirag;

public class Swap {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;

        // Swap numbers
        int temp = a;
        a = b;
        b = temp;

        System.out.println(a + " " + b);

        swap(a,b);

        System.out.println(a + " " + b);

        // Change name

        String name = "Chirag Ganguli";
        changeName(name);
        System.out.println(name);
    }

    private static void changeName(String name) {
        name = "Chirag";    // not changing the object, instead creating a new object
        // the name is not changed here as the values are not passed to the main function
    }

    static void swap(int a, int b) {
        int temp = a;
        a = b;
        b = temp;
        // the numbers are not swapped as the values are not returned
        // this change will only be valid inside this function scope
    }
}

// Refer PassingExample.java to get teh concept of passing