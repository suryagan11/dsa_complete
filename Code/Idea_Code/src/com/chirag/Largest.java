package com.chirag;

import java.util.Scanner;

public class Largest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        // Find largest of the numbers

        // Process 1

        int max = a;
        if(b > max) {
            max = b;
        }
        if(c > max) {
            max = c;
        }

        System.out.println(max);

        // Process 2

        int greater = 0;
        if(a > b) {
            greater = a;
        } else {
            greater = b;
        }
        if(c > max) {
            greater = c;
        }

        System.out.println(greater);

        // Process 3

        System.out.println(Math.max(Math.max(a, b),c));
    }
}
