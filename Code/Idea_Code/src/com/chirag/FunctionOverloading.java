package com.chirag;

// 2 / more func can have same names if the params are different

public class FunctionOverloading {
    public static void main(String[] args) {
        fun(67);
        fun("Chirag");

        // At compile time, it decides which function to run
    }

    static void fun(int a) {
        System.out.print("First: ");
        System.out.println(a);
    }

    static void fun(String name) {
        System.out.print("Second: ");
        System.out.println(name);
    }
}

// Function Overriding happens at run time