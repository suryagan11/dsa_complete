package com.chirag;

public class Conditionals {
    /*
        if (bool expression T / F) {
            // body
        } else {
            // do this
        }
     */

    public static void main(String[] args) {
        int salary = 25000;
        if (salary > 10000) {
            salary += 2000;
        } else if (salary > 20000) {
            salary += 1000;
        } else {
            salary += 500;
        }

        System.out.println("New Salary: " + salary);
    }
}
