package com.chirag;

import java.util.Scanner;

public class DigitCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // Count no of times 5 occurs in a number input by user

        int n = sc.nextInt();
        int count = 0;

        while (n > 0) {
            int rem = n % 10;
            if (rem == 5) {
                count++;
            }
            n = n / 10;
        }

        System.out.println(count);
    }
}
