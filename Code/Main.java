public class Main {     // public: class can be accessed from anywhere
    
    // function is a collection of code

    public static void main(String[] args) {

        // public: class can be accessed from anywhere
        // main function
        // static: objects of Main class that does not change
        
        System.out.println("Hello World");
        System.out.println(args[1]);

        // PS D:\Obsidian_Vaults\DSA_Complete\Code> javac .\Main.java 
        // PS D:\Obsidian_Vaults\DSA_Complete\Code> java Main 30 "Chirag"
        // Hello World
        // Chirag

        // To get .class file in another dir --> javac -d .. Main.java
    }
}