package com.chirag;

public class SearchInRange {
    public static void main(String[] args) {
        int[] arr = {1,4,45,18,12,-7,3,14,28};
        int target = 3;
        System.out.println(linearSearch(arr, target, 1,9));
    }

    static int linearSearch(int[] arr, int target, int start, int end) {
        if(arr.length == 0) {
            return Integer.MAX_VALUE;
        }

        for (int index = start; index <= end; index++) {
            // check for element at every index if it is = target
            int element = arr[index];
            if(element == target) {
                return index;
            }
        }

        // this line will execute if non of the return stmt above have executed
        // target NOT FOUND
        return Integer.MAX_VALUE;
    }
}
