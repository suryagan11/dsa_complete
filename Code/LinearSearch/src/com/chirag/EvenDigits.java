package com.chirag;

// https://leetcode.com/problems/find-numbers-with-even-number-of-digits/
public class EvenDigits {
    public static void main(String[] args) {
        int[] nums = {12, 345, 2,6,7896};
        System.out.println(findNumbers(nums));
        // System.out.println(digits(54617));
    }

    static int findNumbers(int[] nums) {
        int count = 0;
        for(int num : nums) {
            if(even(num)) {
                count++;
            }
        }
        return count;
    }

    // to check if num contain even digits
    private static boolean even(int num) {
        // 1. count no of digits
        // 2. convert to string & calculate length

        int numOfDigits = digits(num);
        if (numOfDigits % 2 == 0) {
            return true;
        }
        return false;
        // return numOfDigits % 2 == 0
    }

    // count no of digits in a number
    static int digits(int num) {
        if(num < 0) {
            num = num * -1;
        }

        if(num == 0) {
            return 1;
        }

        int count = 0;

        while(num > 0) {
            count++;
            num /= 10;
        }

        return count;
    }

    // SHORTCUT to calculate no of digits
    static int digits2(int num) {
        if(num < 0) {
            num = num * -1;
        }

        if(num == 0) {
            return 1;
        }

        return (int)(Math.log10(num)) + 1;
    }
}
