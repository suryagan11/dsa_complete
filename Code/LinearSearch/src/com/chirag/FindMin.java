package com.chirag;

public class FindMin {
    public static void main(String[] args) {
        int[] arr = {23, 45, 1, 2, 19, 16, -3, -11, 28, -71};
        System.out.println(min(arr));
    }

    // assume not an empty array
    static int min(int[] arr) {
        int ans = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] < ans) {
                ans = arr[i];
            }
        }
        return ans;
    }
}
