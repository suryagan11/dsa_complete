package com.chirag;

// https://leetcode.com/problems/richest-customer-wealth/
public class maxWealth {
    public static void main(String[] args) {

    }
    public int maximumWealth(int[][] accounts) {
        // person (row); account (col)
        int ans = Integer.MIN_VALUE;

        for(int person = 0; person < accounts.length; person++) {
            // when you start a new col, take a new sum for tht row
            int sum = 0;
            for (int account = 0; account < accounts[person].length; account++) {
                sum += accounts[person][account];
            }

            // now we have sum of accounts of person
            if(sum > ans) {
                ans = sum;
            }
        }
        return ans;
    }
}
