package com.chirag;

public class LinearSearch {

    public static void main(String[] args) {
        int[] nums = {23, 45, 1, 2, 19, 16, -3, -11, 28, -71};
        int target = 19;
        int ans = linearSearch(nums, target);
        int ans2 = linearSearch2(nums, target);

        System.out.println(ans);
        System.out.println(ans2);
        System.out.println(linearSearch3(nums,target));
    }

    // search in the array: return the index if item found else return -1
    static int linearSearch(int[] arr, int target) {
        if(arr.length == 0) {
            return Integer.MAX_VALUE;
        }

        for (int index = 0; index < arr.length; index++) {
            // check for element at every index if it is = target
            int element = arr[index];
            if(element == target) {
                return index;
            }
        }

        // this line will execute if non of the return stmt above have executed
        // target NOT FOUND
        return Integer.MAX_VALUE;
    }

    // search the target & return the element
    static int linearSearch2(int[] arr, int target) {
        if(arr.length == 0) {
            return Integer.MAX_VALUE;
        }

        for (int element : arr) {
            // check for element at every index if it is = target
            if (element == target) {
                return element;
            }
        }

        // this line will execute if non of the return stmt above have executed
        // target NOT FOUND
        return Integer.MAX_VALUE;
    }

    // search the target & return true or false
    static boolean linearSearch3(int[] arr, int target) {
        if(arr.length == 0) {
            return false;
        }

        for (int element : arr) {
            // check for element at every index if it is = target
            if (element == target) {
                return true;
            }
        }

        // this line will execute if non of the return stmt above have executed
        // target NOT FOUND
        return false;
    }
}
